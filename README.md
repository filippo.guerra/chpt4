# Chpt4

In this repository, there are the tables with information regarding Chapter 4 "Sexually dimorphic pigmentation of Nasonia vitripennis is instructed by Doublesex responsive elements on genes of the Yellow family" of the thesis "The role of doublesex in shaping temporal, spatial and species-specific sex differentiation in Nasonia wasps"

Supplementary Table 1: Primers used in qPCR analysis. 
If not specified otherwise in the column “Target”, the primers can be used on both Nasonia vitripennis and Muscidifurax raptorellus cDNA.


Supplementary Table 2: Primers used in the synthesis of template for dsRNA. 
The primers were used on both Nasonia vitripennis and Muscidifurax raptorellus cDNA to produce species-specific amplicons.

Supplementary Table 3: Primers designed to produce the amplicon library used in DAP-qPCR. 
